﻿using Id3TagEditor.Entities;
using Id3TagEditor.Exceptions;
using Id3TagEditor.Extensions;
using Id3TagEditor.Services;
using Microsoft.AspNetCore.Mvc;

namespace Id3TagEditor.Controllers
{
    [Route("api/files")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly FilesService _fileService;

        public FilesController(FilesService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public FileListResponse ListFiles(
            [FromQuery] string? path)
        {
            return _fileService.GetFiles(path);
        }

        [HttpGet("tag")]
        public Id3TagResponse GetTags(
            [FromQuery] string path)
        {
            return _fileService.GetId3TagOfFile(path);
        }

        [HttpPost("tag")]
        public void UpdateTags(
            [FromQuery] string path,
            [FromBody] Id3TagData tagData)
        {
            _fileService.WriteId3TagToFile(path, tagData)
                .OrThrowIfFalse(() => new HttpResponseException(
                    StatusCodes.Status500InternalServerError,
                    "Failed to update requested file"));
        }
    }
}
