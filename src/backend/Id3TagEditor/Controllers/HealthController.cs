﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Id3TagEditor.Controllers
{
    [Route("api/health")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public string GetHealthCheck()
        {
            return "OK";
        }

        [HttpGet("alive")]
        public IActionResult IsAlive()
        {
            return NoContent();
        }
    }
}
