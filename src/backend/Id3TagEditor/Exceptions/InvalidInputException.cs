﻿namespace Id3TagEditor.Exceptions
{
    public class InvalidInputException : HttpResponseException
    {
        public InvalidInputException(string message) : base(StatusCodes.Status400BadRequest, message)
        {
        }
    }
}
