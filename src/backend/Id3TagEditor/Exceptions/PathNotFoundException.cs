﻿namespace Id3TagEditor.Exceptions
{
    public class PathNotFoundException : HttpResponseException
    {
        public PathNotFoundException()
            : base(StatusCodes.Status404NotFound, "The given path could not be found.")
        {
        }
        public PathNotFoundException(string path)
            : base(StatusCodes.Status404NotFound, $"\"{path}\" could not be found.")
        {
        }
    }
}
