﻿namespace Id3TagEditor.Exceptions
{
    public class HttpResponseException : Exception
    {
        public HttpResponseException(int errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }

        public int ErrorCode { get; }
    }
}
