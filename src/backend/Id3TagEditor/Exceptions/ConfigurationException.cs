﻿namespace Id3TagEditor.Exceptions
{
    public class ConfigurationException : HttpResponseException
    {
        public ConfigurationException(string message) : base(StatusCodes.Status500InternalServerError, message)
        {
        }
    }
}
