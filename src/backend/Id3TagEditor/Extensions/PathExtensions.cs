﻿using System.Runtime.InteropServices;
using Id3TagEditor.Exceptions;

namespace Id3TagEditor.Extensions
{
    public static class PathExtensions
    {
        static PathExtensions()
        {
            _isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        }

        private static readonly bool _isWindows;
        public static bool IsWindows => _isWindows;

        /// <summary>
        /// Combines the given base path with attended path elements.
        /// If the resulting path results in it pointing outside of the given
        /// basePath, an InvalidInputException is thrown.
        /// </summary>
        /// <param name="basePath">Base Path</param>
        /// <param name="pathArgs">Path elements to join</param>
        /// <returns>Joined, absolute path</returns>
        /// <exception cref="InvalidInputException">
        /// The absolute path resulting from the join points outside of the basePath.
        /// </exception>
        public static string SafeCombinePath(this string basePath, params string?[] pathArgs)
        {
            var fullBasePath = Path.GetFullPath(basePath);
            if (pathArgs == null || pathArgs.Any(a => a == null))
            {
                return fullBasePath;
            }

            var fullCombinedPath = Path.Combine(pathArgs.Prepend(fullBasePath).ToArray());

            if (!fullCombinedPath.StartsWith(fullBasePath))
            {
                throw new InvalidInputException("The provided path is invalid. It must not start with a \"/\".");
            }
            return fullCombinedPath;
        }

        public static string GetRelativePath(this string path, string root)
        {
            string absoluteInputPath = Path.GetFullPath(path);
            string absoluteRootPath = Path.GetFullPath(root);
            return absoluteInputPath.Substring(absoluteInputPath.IndexOf(absoluteRootPath) + absoluteRootPath.Length);
        }
    }
}
