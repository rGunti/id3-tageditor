﻿using Id3.Frames;

namespace Id3TagEditor.Extensions
{
    public static class Id3Extensions
    {
        public static string? RemoveNullByte(this string? s)
        {
            if (s == null)
            {
                return null;
            }
            if (s.EndsWith('\0'))
            {
                return s.Replace("\0", "");
            }
            return s;
        }

        public static string? RemoveNullByte(this TextFrameBase<string>? tag)
        {
            if (tag == null)
            {
                return null;
            }
            return tag.Value.RemoveNullByte();
        }

        public static string? RemoveNullByte(this UrlLinkFrame? tag)
        {
            if (tag == null)
            {
                return null;
            }
            return tag.Url.RemoveNullByte();
        }

        public static string[] RemoveNullBytes(this ListTextFrame tag)
        {
            if (tag == null)
            {
                return Array.Empty<string>();
            }
            return tag.Value
                .Select(t => t?.RemoveNullByte())
                .Where(t => t != null)
                .ToArray();
        }

        public static void AddRange(this IList<string> list, IEnumerable<string>? items)
        {
            if (items == null)
            {
                return;
            }
            foreach (var item in items)
            {
                list.Add(item);
            }
        }

        public static void SetIfNotNull<T, TValue>(this T frame, TValue? value)
            where T : TextFrameBase<TValue>
        {
            if (value != null)
            {
                frame.EncodingType = Id3.Id3TextEncoding.Unicode;
                frame.Value = value;
            }
        }

        public static void SetIfNotNull(this CopyrightUrlFrame frame, string? value)
        {
            if (value != null)
            {
                frame.Url = value;
            }
        }
    }
}
