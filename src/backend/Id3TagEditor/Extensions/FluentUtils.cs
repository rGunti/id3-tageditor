﻿namespace Id3TagEditor.Extensions
{
    public static class FluentUtils
    {
        public static void OrThrowIfFalse(
            this bool boolVal,
            Func<Exception> exceptionFn)
        {
            if (!boolVal)
            {
                throw exceptionFn();
            }
        }

        public static int? ToInt(
            this string? str)
        {
            if (str == null || !int.TryParse(str, out int i))
            {
                return null;
            }
            return i;
        }
    }
}
