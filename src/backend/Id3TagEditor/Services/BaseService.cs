﻿namespace Id3TagEditor.Services
{
    public abstract class BaseService<T>
        where T : BaseService<T>
    {
        protected readonly ILogger<T> _log;

        protected BaseService(ILogger<T> logger)
        {
            _log = logger;
        }
    }
}
