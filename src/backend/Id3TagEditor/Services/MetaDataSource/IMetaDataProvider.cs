﻿using Id3TagEditor.Entities;

namespace Id3TagEditor.Services.MetaDataSource
{
    public interface IMetaDataProvider
    {
        string[] SupportedFileExtensions { get; }

        Id3TagData? ReadMetaDataFromFile(string filePath);

        bool SaveMetaDataFromFile(string filePath, Id3TagData tagData);
    }
}
