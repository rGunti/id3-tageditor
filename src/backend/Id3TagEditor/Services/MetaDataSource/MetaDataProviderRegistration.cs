﻿namespace Id3TagEditor.Services.MetaDataSource
{
    public static class MetaDataProviderRegistration
    {
        public static IServiceCollection RegisterMetaDataRegistration(
            this IServiceCollection services)
        {
            return services
                // Factory
                .AddSingleton<IMetaDataProviderFactory, MetaDataProviderFactory>()
                // Providers
                .AddSingleton<IMetaDataProvider, Mp3MetaDataProvider>()
                .AddSingleton<IMetaDataProvider, FlacMetaDataProvider>();
        }
    }
}
