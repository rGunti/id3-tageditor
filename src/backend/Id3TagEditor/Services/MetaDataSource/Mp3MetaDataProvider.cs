﻿using Id3;
using Id3.Frames;
using Id3TagEditor.Entities;
using Id3TagEditor.Extensions;

namespace Id3TagEditor.Services.MetaDataSource
{
    public class Mp3MetaDataProvider : IMetaDataProvider
    {
        private readonly ILogger<Mp3MetaDataProvider> _log;

        public Mp3MetaDataProvider(ILogger<Mp3MetaDataProvider> log)
        {
            _log = log;
        }

        public string[] SupportedFileExtensions => new string[] { ".mp3" };

        public Id3TagData? ReadMetaDataFromFile(string filePath)
        {
            _log.LogDebug("Reading meta data from {FilePath}", filePath);
            using (var mp3 = new Mp3(filePath))
            {
                var tags = mp3.GetAllTags();
                if (tags?.Any() ?? false)
                {
                    return GetTagData(tags);
                }
                _log.LogWarning("The file \"{Path}\" doesn't have any recognisable ID3 tags", filePath);
                return new Id3TagData();
            }
        }

        private static Id3TagData GetTagData(IEnumerable<Id3Tag> tags)
        {
            var tag = tags
                .OrderByDescending(t => t.Version)
                .First();
            return new Id3TagData
            {
                TagVersion = tag.Version.ToString(),
                Artists = tag.Artists.RemoveNullBytes(),
                Album = tag.Album.RemoveNullByte(),
                Band = tag.Band.RemoveNullByte(),
                Bpm = tag.BeatsPerMinute,
                Composers = tag.Composers.RemoveNullBytes(),
                Conductor = tag.Conductor.RemoveNullByte(),
                Copyright = tag.Copyright.RemoveNullByte(),
                CopyrightUrl = tag.CopyrightUrl.RemoveNullByte(),
                Encoder = tag.Encoder.RemoveNullByte(),
                FileOwner = tag.FileOwner.RemoveNullByte(),
                FileType = tag.FileType?.Value.ToString(),
                Genre = tag.Genre.RemoveNullByte(),
                Lyricists = tag.Lyricists.RemoveNullBytes(),
                Publisher = tag.Publisher.RemoveNullByte(),
                Title = tag.Title.RemoveNullByte(),
                Track = tag.Track?.Value,
                TrackCount = tag.Track?.TrackCount,
                Year = tag.Year
            };
        }

        public bool SaveMetaDataFromFile(string filePath, Id3TagData tagData)
        {
            using var mp3 = new Mp3(filePath, Mp3Permissions.ReadWrite);
            var existingTag = mp3.GetTag(Id3TagFamily.Version2X);
            if (existingTag == null)
            {
                var tag = ConvertToTag(tagData);
                return mp3.WriteTag(tag, Id3Version.V23, WriteConflictAction.NoAction);
            }
            else
            {
                return mp3.UpdateTag(ConvertToTag(tagData, existingTag));
            }
        }

        private static Id3Tag ConvertToTag(Id3TagData tagData, Id3Tag? tag = null)
        {
            if (tag == null)
            {
                tag = new Id3Tag();
            }
            tag.Artists.Value.AddRange(tagData.Artists);
            tag.Album.SetIfNotNull(tagData.Album);
            tag.Band.SetIfNotNull(tagData.Band);
            tag.BeatsPerMinute.SetIfNotNull(tagData.Bpm);
            tag.Composers.Value.AddRange(tagData.Composers);
            tag.Conductor.SetIfNotNull(tagData.Conductor);
            tag.Copyright.SetIfNotNull(tagData.Copyright);
            tag.CopyrightUrl.SetIfNotNull(tagData.CopyrightUrl);
            tag.FileOwner.SetIfNotNull(tagData.FileOwner);
            tag.Genre.SetIfNotNull(tagData.Genre);
            tag.Lyricists.Value.AddRange(tagData.Lyricists);
            tag.Publisher.SetIfNotNull(tagData.Publisher);
            tag.Title.SetIfNotNull(tagData.Title);
            tag.Track = ConvertToTrackFrame(tagData.Track, tagData.TrackCount);
            tag.Year.SetIfNotNull(tagData.Year);
            return tag;
        }

        private static TrackFrame ConvertToTrackFrame(int? track, int? trackCount)
        {
            var frame = new TrackFrame();
            if (track.HasValue)
            {
                frame.Value = track.Value;
            }
            if (trackCount.HasValue)
            {
                frame.TrackCount = trackCount.Value;
            }
            return frame;
        }
    }
}
