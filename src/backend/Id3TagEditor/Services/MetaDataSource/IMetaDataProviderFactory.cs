﻿namespace Id3TagEditor.Services.MetaDataSource
{
    public interface IMetaDataProviderFactory
    {
        IMetaDataProvider CreateMetaDataProvider(string filePath);
    }

    public class MetaDataProviderFactory : IMetaDataProviderFactory
    {
        private readonly IReadOnlyDictionary<string, IMetaDataProvider> _providers;

        public MetaDataProviderFactory(IEnumerable<IMetaDataProvider> providers)
        {
            _providers = providers
                .SelectMany(p => p.SupportedFileExtensions
                    .Select(ext => new { Extension = ext.ToLowerInvariant(), Instance = p }))
                .ToDictionary(i => i.Extension, i => i.Instance);
        }

        public IMetaDataProvider CreateMetaDataProvider(string filePath)
        {
            var fileExtensions = Path.GetExtension(filePath)?.ToLowerInvariant();
            if (string.IsNullOrWhiteSpace(fileExtensions))
            {
                throw new ArgumentException($"Argument {nameof(filePath)} contains no detectable file extension.");
            }

            if (!_providers.TryGetValue(fileExtensions, out var provider))
            {
                throw new Exception($"No metadata provider found for file extension {fileExtensions}.");
            }
            return provider;
        }
    }
}
