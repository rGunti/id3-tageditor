﻿using Id3TagEditor.Entities;
using Id3TagEditor.Extensions;
using LibFlacSharp;
using LibFlacSharp.Metadata;

namespace Id3TagEditor.Services.MetaDataSource
{
    internal static class CustomVorbisCommentTypes
    {
        internal const string Conductor = "Conductor";
        internal const string Publisher = "Publisher";
        internal const string Band = "Band";
        internal const string Bpm = "Bpm";
        internal const string CopyrightUrl = "CopyrightUrl";
        internal const string FileOwner = "FileOwner";

        internal const string Year = "date";
    }

    public class FlacMetaDataProvider : IMetaDataProvider
    {
        public string[] SupportedFileExtensions => new string[] { ".flac" };

        public Id3TagData? ReadMetaDataFromFile(string filePath)
        {
            using var flac = new FlacFile(filePath);
            return ConvertFromFile(flac.VorbisComment);
        }

        private static Id3TagData ConvertFromFile(VorbisComment vorbisComment)
        {
            var comments = vorbisComment.CommentList;
            return new Id3TagData
            {
                Title = comments.GetValueOrDefault(VorbisCommentType.Title),
#pragma warning disable CS8601 // Possible null reference assignment.
                Artists = new string[] { comments.GetValueOrDefault(VorbisCommentType.Artist) }
#pragma warning restore CS8601 // Possible null reference assignment.
                    .Where(i => i != null)
                    .ToArray(),
                Album = comments.GetValueOrDefault(VorbisCommentType.Album),
                Copyright = comments.GetValueOrDefault(VorbisCommentType.Copyright),
                Year = comments.GetValueOrDefault(VorbisCommentType.Year).ToInt() ?? comments.GetValueOrDefault(CustomVorbisCommentTypes.Year).ToInt(),
                Track = comments.GetValueOrDefault(VorbisCommentType.TrackNumber).ToInt(),
                TrackCount = comments.GetValueOrDefault(VorbisCommentType.TrackTotal).ToInt(),
                Genre = comments.GetValueOrDefault(VorbisCommentType.Genre),
#pragma warning disable CS8601 // Possible null reference assignment.
                Lyricists = new string[] { comments.GetValueOrDefault(VorbisCommentType.Lyricist) }
#pragma warning restore CS8601 // Possible null reference assignment.
                    .Where(i => i != null)
                    .ToArray(),
#pragma warning disable CS8601 // Possible null reference assignment.
                Composers = new string[] { comments.GetValueOrDefault(VorbisCommentType.Composer) }
#pragma warning restore CS8601 // Possible null reference assignment.
                    .Where(i => i != null)
                    .ToArray(),
                Conductor = comments.GetValueOrDefault(CustomVorbisCommentTypes.Conductor),
                Publisher = comments.GetValueOrDefault(CustomVorbisCommentTypes.Publisher),
                Band = comments.GetValueOrDefault(CustomVorbisCommentTypes.Band),
                Bpm = comments.GetValueOrDefault(CustomVorbisCommentTypes.Bpm).ToInt(),
                CopyrightUrl = comments.GetValueOrDefault(CustomVorbisCommentTypes.CopyrightUrl),
                FileOwner = comments.GetValueOrDefault(CustomVorbisCommentTypes.Publisher),
                TagVersion = vorbisComment.VendorString,
                FileType = "FLAC",
                Encoder = "FLAC",
                ExtendedMetaData = comments,
            };
        }

        public bool SaveMetaDataFromFile(string filePath, Id3TagData tagData)
        {
            var tempFile = GetNewTemporaryFile();
            using (var flac = new FlacFile(filePath))
            {
                UpdateVorbisCommentsFromTags(flac.VorbisComment, tagData);
                using var tempFs = new FileStream(tempFile, FileMode.Create, FileAccess.ReadWrite);
                flac.SaveAsync(tempFs)
                    .Wait(30000)
                    .OrThrowIfFalse(() => new Exception("Failed to write file in time (30 sec)"));
            }
            System.IO.File.Move(tempFile, filePath, true);
            return true;
        }

        private static void UpdateVorbisCommentsFromTags(VorbisComment vorbisComment, Id3TagData tagData)
        {
            var comments = vorbisComment.CommentList;
            comments[VorbisCommentType.Title] = tagData.Title;
            comments[VorbisCommentType.Album] = tagData.Album;
            comments[VorbisCommentType.Copyright] = tagData.Copyright;
            comments[VorbisCommentType.Year] = tagData.Year?.ToString();
            comments[CustomVorbisCommentTypes.Year] = tagData.Year?.ToString();
            comments[VorbisCommentType.TrackNumber] = tagData.Track?.ToString();
            comments[VorbisCommentType.TrackTotal] = tagData.TrackCount?.ToString();
            comments[VorbisCommentType.Genre] = tagData.Genre;

            comments[VorbisCommentType.Artist] = string.Join(",", tagData.Artists ?? Array.Empty<string>());
            comments[VorbisCommentType.Lyrics] = string.Join(",", tagData.Lyricists ?? Array.Empty<string>());
            comments[VorbisCommentType.Composer] = string.Join(",", tagData.Composers ?? Array.Empty<string>());

            comments[CustomVorbisCommentTypes.Conductor] = tagData.Conductor;
            comments[CustomVorbisCommentTypes.Publisher] = tagData.Publisher;
            comments[CustomVorbisCommentTypes.Band] = tagData.Band;
            comments[CustomVorbisCommentTypes.Bpm] = tagData.Bpm?.ToString();
            comments[CustomVorbisCommentTypes.CopyrightUrl] = tagData.CopyrightUrl;
        }

        private static string GetNewTemporaryFile() => Path.GetTempFileName();
    }
}
