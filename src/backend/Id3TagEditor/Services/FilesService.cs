﻿using Id3;
using Id3.Frames;
using Id3TagEditor.Entities;
using Id3TagEditor.Entities.Config;
using Id3TagEditor.Exceptions;
using Id3TagEditor.Extensions;
using Id3TagEditor.Services.MetaDataSource;
using Microsoft.Extensions.Options;

namespace Id3TagEditor.Services
{
    public class FilesService : BaseService<FilesService>
    {
        private readonly IOptions<EditorConfiguration> _config;
        private readonly string _configDir;
        private readonly string[] _filterForExtensions;
        private readonly IMetaDataProviderFactory _providerFactory;

        public FilesService(
            ILogger<FilesService> logger,
            IOptions<EditorConfiguration> editorConfiguration,
            IMetaDataProviderFactory providerFactory) : base(logger)
        {
            _config = editorConfiguration;
            _providerFactory = providerFactory;

            var configDir = editorConfiguration.Value.Directory;
            if (configDir == null)
            {
                _log.LogError("No directory has been configured to read files from. Configure them via config file or environment variable: {ConfigProperty}",
                    $"{EditorConfiguration.PROP}.{nameof(EditorConfiguration.Directory)}");
                throw new ConfigurationException("No directory has been configured to read files from.");
            }
            if (!Directory.Exists(configDir))
            {
                _log.LogError(
                    "The configured base directory \"{Path}\" does not exist!",
                    configDir);
                throw new ConfigurationException($"The configured directory \"{configDir}\" doesn't exist.");
            }
            _configDir = configDir;
            _filterForExtensions = _config.Value.FilterForExtensions ?? Array.Empty<string>();
        }

        public FileListResponse GetFiles(string? subDirectory)
        {
            string fullPath = _configDir.SafeCombinePath(subDirectory);
            if (!Directory.Exists(fullPath))
            {
                _log.LogWarning("The directory \"{Path}\" doesn't exist but was requested to be listed", fullPath);
                throw new PathNotFoundException(fullPath.GetRelativePath(_configDir));
            }

            _log.LogDebug("Directory Listing for \"{Path}\" requested", fullPath);
            IEnumerable<string> files = Directory.GetFiles(fullPath) ?? Array.Empty<string>();
            IEnumerable<string> dirs = Directory.GetDirectories(fullPath) ?? Array.Empty<string>();
            if (_filterForExtensions.Length > 0)
            {
                files = files
                    .Where(f => Directory.Exists(f) || _filterForExtensions.Contains(Path.GetExtension(f)?.ToLowerInvariant()));
            }
            return new(
                fullPath.GetRelativePath(_configDir),
                files.Select(p => new Entities.File()
                {
                    Name = Path.GetFileName(p),
                    Size = new FileInfo(p).Length,
                }).OrderBy(f => f.Name?.ToLowerInvariant()),
                dirs.Select(d => Path.GetFileName(d)).OrderBy(d => d));
        }

        public Id3TagResponse GetId3TagOfFile(string relativePath)
        {
            var fullPath = _configDir.SafeCombinePath(relativePath);
            if (!System.IO.File.Exists(fullPath))
            {
                _log.LogWarning("The file \"{Path}\" doesn't exist but was requested to be read from", fullPath);
                throw new PathNotFoundException(fullPath.GetRelativePath(relativePath));
            }

            var provider = _providerFactory.CreateMetaDataProvider(fullPath);
            return new Id3TagResponse
            {
                Path = fullPath.GetRelativePath(_configDir),
                Data = provider.ReadMetaDataFromFile(fullPath) ?? new Id3TagData()
            };
        }

        public bool WriteId3TagToFile(string relativePath, Id3TagData tagData)
        {
            var fullPath = _configDir.SafeCombinePath(relativePath);
            if (!System.IO.File.Exists(fullPath))
            {
                _log.LogWarning("The file \"{Path}\" doesn't exist but was requested to be written to", fullPath);
                throw new PathNotFoundException(fullPath.GetRelativePath(relativePath));
            }
            
            var provider = _providerFactory.CreateMetaDataProvider(fullPath);
            return provider.SaveMetaDataFromFile(fullPath, tagData);
        }
    }
}
