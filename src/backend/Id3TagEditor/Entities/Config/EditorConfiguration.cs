﻿namespace Id3TagEditor.Entities.Config
{
    public class EditorConfiguration
    {
        public const string PROP = "ID3TE";

        public string? Directory { get; set; }
        public string[] FilterForExtensions { get; set; }
    }
}
