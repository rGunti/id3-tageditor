﻿namespace Id3TagEditor.Entities
{
    public abstract class FileServiceResponse
    {
        public string Path { get; set; }
    }

    public class FileListResponse : FileServiceResponse
    {
        public FileListResponse()
        {
        }

        public FileListResponse(
            string path,
            IEnumerable<File> files,
            IEnumerable<string> subDirectories)
        {
            Path = path;
            Files = files.ToArray();
            SubDirectories = subDirectories.ToArray();
        }

        public File[] Files { get; set; }
        public string[] SubDirectories { get; set; }
    }

    public class File
    {
        public string Name { get; set; }
        public long Size { get; set; }
    }

    public class Id3TagResponse : FileServiceResponse
    {
        public Id3TagData Data { get; set; }
    }

    public class Id3TagData
    {
        public string? TagVersion { get; set; }
        public string[]? Artists { get; set; }
        public string? Album { get; set; }
        public string? Band { get; set; }
        public int? Bpm { get; set; }
        public string[]? Composers { get; set; }
        public string? Conductor { get; set; }
        public string? Copyright { get; set; }
        public string? CopyrightUrl { get; set; }
        public string? Encoder { get; set; }
        public string? FileOwner { get; set; }
        public string? FileType { get; set; }
        public string? Genre { get; set; }
        public string[]? Lyricists { get; set; }
        public string? Publisher { get; set; }
        public string? Title { get; set; }
        public int? Track { get; set; }
        public int? TrackCount { get; set; }
        public int? Year { get; set; }
        public Dictionary<string, string> ExtendedMetaData { get; set; } = new Dictionary<string, string>();
    }
}
