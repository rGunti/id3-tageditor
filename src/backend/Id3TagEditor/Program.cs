using Id3TagEditor.Entities.Config;
using Id3TagEditor.Filters;
using Id3TagEditor.Services;
using Id3TagEditor.Services.MetaDataSource;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((ctx, lc) => lc
    .ReadFrom.Configuration(ctx.Configuration));
var services = builder.Services;

// Add services to the container.
services.AddCors(o => {
    o.AddDefaultPolicy(b => b
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());
});
services.AddControllers(o =>
{
    o.Filters.Add<HttpResponseExceptionFilter>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

services.Configure<EditorConfiguration>(
    builder.Configuration.GetRequiredSection(EditorConfiguration.PROP));
services.AddTransient<FilesService>();
services.RegisterMetaDataRegistration();

var app = builder.Build();
app.UseCors();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
