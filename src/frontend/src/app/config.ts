import { InjectionToken } from "@angular/core";

export const T_API = new InjectionToken<string>('BackendApi');
