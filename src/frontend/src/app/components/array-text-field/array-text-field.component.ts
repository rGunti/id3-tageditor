import { Component, ChangeDetectionStrategy, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-array-text-field',
  templateUrl: './array-text-field.component.html',
  styleUrls: ['./array-text-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ArrayTextFieldComponent,
    }
  ]
})
export class ArrayTextFieldComponent implements ControlValueAccessor, OnDestroy {

  private onValueUpdateSub: Subscription | null = null;

  onChange: ((newValue: any) => any) = (_) => {};
  onTouched: () => any = () => {};

  disabled: boolean = false;
  touched: boolean = false;

  form: FormArray;

  @Input() label: string = 'Items';
  @Input() singleItemLabel: string = 'Item';

  constructor(private readonly fb: FormBuilder) {
    this.form = fb.array([]);
    this.updateForm([]);
  }

  ngOnDestroy(): void {
    if (this.onValueUpdateSub) {
      this.onValueUpdateSub.unsubscribe();
    }
  }

  get controls(): FormControl[] {
    return this.form.controls as FormControl[];
  }

  get value(): any[] {
    return this.form.value;
  }

  setItems(items: string[]): void {
    this.updateForm(items);
    this.form.setValue(items);
  }

  get arraySize(): number {
    return this.form.length || 0;
  }

  get addItemLabel(): string {
    return `Add ${this.singleItemLabel || 'Item'}`;
  }
  get deleteItemLabel(): string {
    return `Delete ${this.singleItemLabel || 'Item'}`;
  }
  get clearItemLabel(): string {
    return `Clear ${this.label || 'Items'}`;
  }

  addItem($event: MouseEvent): void {
    const array = [
      ...this.form.value,
      ''
    ];
    this.setItems(array);
    this.onChange(this.value);
  }
  deleteItem($event: MouseEvent, index: number): void {
    const formArray: any[] = this.form.value;
    const array = [
      ...formArray.slice(0, index),
      ...formArray.slice(index + 1)
    ];
    this.setItems(array);
    this.onChange(this.value);
  }
  clearItems($event: MouseEvent): void {
    this.setItems([]);
    this.onChange(this.value);
  }

  writeValue(obj: any): void {
    this.setItems(obj as any[]);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  private updateForm(array: any[]): void {
    this.form = this.fb.array(array.map((i) => this.fb.control(i)));
    this.onValueUpdateSub = this.form.valueChanges.subscribe((_) => this.onChange(this.value));
  }

  private markAsTouched(): void {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

}
