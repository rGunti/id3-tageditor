import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

export declare type InfoBoxState = 'note' | 'info' | 'warning' | 'danger';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoBoxComponent {

  @Input() level: InfoBoxState = 'info';

  @Input() customTitle: string | null | undefined = null;
  @Input() customIcon: string | null | undefined = null;
  @Input() useSvgForIcon: boolean = false;

  constructor() { }

  get customClasses(): string[] {
    return ['info-box-card-container', `${this.level}`];
  }

  get icon(): string {
    if (this.customIcon !== null || this.customIcon !== undefined) {
      return this.customIcon!;
    }

    switch (this.level) {
      case 'note': return 'lightbulb';
      case 'info': return 'info';
      case 'warning': return 'warning';
      case 'danger': return 'report';
      default: return 'bug_report';
    }
  }

  get renderedTitle(): string {
    if (this.customTitle === null || this.customTitle === undefined) {
      switch (this.level) {
        case 'note': return 'Note';
        case 'info': return 'Information';
        case 'warning': return 'Warning';
        case 'danger': return 'Danger';
        default: return '???';
      }
    }
    return this.customTitle;
  }

}
