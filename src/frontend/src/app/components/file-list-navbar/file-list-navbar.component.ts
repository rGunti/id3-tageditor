import { Component } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { Store } from '@ngrx/store';
import { map, Observable, tap } from 'rxjs';
import { GlobalState } from 'src/app/state';
import { fileListActions } from 'src/app/state/file-list';

@Component({
  selector: 'app-file-list-navbar',
  templateUrl: './file-list-navbar.component.html',
  styleUrls: ['./file-list-navbar.component.scss']
})
export class FileListNavbarComponent {

  currentPath$: Observable<string>;
  loadingBarMode$: Observable<ProgressBarMode>;

  private currentPath: string = '';

  constructor(
    private readonly store: Store<GlobalState>,
  ) {
    this.currentPath$ = store.select('fileList').pipe(
      map(s => s.currentPath || '/'),
      tap(p => {
        this.currentPath = p;
      })
    );

    this.loadingBarMode$ = store.select('fileList').pipe(
      map(s => s.isLoading),
      map(isLoading => {
        let state: ProgressBarMode = 'determinate';
        if (isLoading) {
          state = 'indeterminate';
        }
        return state;
      })
    );
  }

  get isTopFolder(): boolean {
    return this.currentPath === '/';
  }

  navigateToFolder(path: string): void {
    this.store.dispatch(fileListActions.fileListUpdate({
      path
    }));
  }

  navigateBack(): void {
    this.navigateToFolder([...this.currentPath.split('/'), '..'].join('/'));
  }
  navigateToRoot(): void {
    this.navigateToFolder('');
  }

}
