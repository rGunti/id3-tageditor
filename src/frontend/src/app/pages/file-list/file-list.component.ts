import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { filter, map, Observable, Subscription, tap } from 'rxjs';
import { File } from 'src/app/backend';
import { TagEditorDialogComponent } from 'src/app/dialogs/tag-editor-dialog/tag-editor-dialog.component';
import { GlobalState } from 'src/app/state';
import { editorActions } from 'src/app/state/editor';
import { fileListActions, FileListState } from 'src/app/state/file-list';

@Component({
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss']
})
export class FileListComponent implements OnInit, OnDestroy {

  fileListState$: Observable<FileListState>;
  subDirectories$: Observable<string[]>;
  files$: Observable<File[]>;

  currentPath: string = '';
  isLoading: boolean = false;

  private subs: Subscription[] = [];

  constructor(
    private readonly store: Store<GlobalState>,
    private readonly snackBar: MatSnackBar,
    private readonly dialog: MatDialog,
  ) {
    this.fileListState$ = store.select('fileList').pipe(
      tap(s => {
        this.currentPath = s.currentPath;
      })
    );
    this.subDirectories$ = this.fileListState$.pipe(
      map(s => s.subDirectories)
    );
    this.files$ = this.fileListState$.pipe(
      map(s => s.files)
    );

    this.subs.push(
      this.fileListState$.pipe(
        map(s => s.failureMessage),
        filter(s => s !== null)
      ).subscribe(err => {
        this.snackBar.open(`Failed to load data: ${err}`, 'OK', { duration: 2500 });
      }));
  }

  get isTopFolder(): boolean {
    return this.currentPath === '';
  }

  ngOnInit(): void {
    this.navigateToFolder('');
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  navigateToFolder(dir: string, baseDir: string = ''): void {
    this.store.dispatch(fileListActions.fileListUpdate({
      path: [baseDir, dir].join('/')
    }));
  }

  openFile(file: File): void {
    this.store.dispatch(editorActions.editorUpdate({
      path: [this.currentPath, file.name].join('/')
    }));
  }

}
