import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Id3TagData, Id3TagResponse } from 'src/app/backend';

@Component({
  templateUrl: './tag-editor-dialog.component.html',
  styleUrls: ['./tag-editor-dialog.component.scss']
})
export class TagEditorDialogComponent implements OnInit {

  form: FormGroup;

  constructor(
    private readonly dialogRef: MatDialogRef<TagEditorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly params: Id3TagResponse | null,
    fb: FormBuilder,
  ) {
    this.form = fb.group({
      // Track
      title: [''],
      artists: [[]],
      album: [''],
      genre: [''],
      track: [null],
      trackCount: [null],
      bpm: [null],
      year: [null],
      band: [''],
      // Composition
      composers: [[]],
      lyricists: [[]],
      // Publication
      publisher: [''],
      copyright: [''],
      copyrightUrl: [''],
    });
    if (this.params && this.params.data) {
      this.form.patchValue(this.params.data);
    }
  }

  get tags(): Id3TagData | null {
    return this.params?.data || null;
  }

  get outputValue(): Id3TagResponse {
    return {
      path: this.params!.path,
      data: this.form.value
    };
  }

  ngOnInit(): void {
  }

  public static open(dialog: MatDialog, params: Id3TagResponse | null): Observable<Id3TagResponse> {
    return dialog.open(TagEditorDialogComponent, {
      data: params
    }).afterClosed();
  }

  onDebugClicked() {
    console.log('Got Form Value', this.form.value);
  }

}
