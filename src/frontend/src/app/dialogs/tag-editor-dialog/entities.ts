import { File } from 'src/app/backend';

export interface TagEditorDialogInput {
  path: string;
  file: File;
}
