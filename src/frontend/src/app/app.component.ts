import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable, switchMap, timer } from 'rxjs';
import { GlobalState } from './state';
import { HealthCheckState } from './state/health-check';
import { healthCheck } from './state/health-check/health-check.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  healthCheckState$: Observable<HealthCheckState>;
  healthCheckIcon$: Observable<string>;
  healthCheckTooltip$: Observable<string>;

  constructor(
    private readonly store: Store<GlobalState>
  ) {
    this.healthCheckState$ = store.select('healthCheck');
    this.healthCheckIcon$ = this.healthCheckState$.pipe(
      map(s => {
        if (!!s) {
          if (s.isLoading) {
            return 'refresh';
          } else if (s.isOnline) {
            return 'check';
          }
        }
        return 'error_outline';
      })
    );
    this.healthCheckTooltip$ = this.healthCheckState$
      .pipe(map(s => {
        if (!s) {
          return 'No Health Check data';
        } else if (s.isLoading) {
          return 'Refreshing ...'
        } else if (s.isOnline) {
          return 'Backend online';
        } else if (!!s.failureMessage) {
          return s.failureMessage;
        }
        return 'State unknown';
      }));

    timer(0, 5000).subscribe({
      next: () => this.store.dispatch(healthCheck(null))
    });
  }

  updateHealthCheck(): void {
    this.store.dispatch(healthCheck(null));
  }
}
