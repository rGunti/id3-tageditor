import { HttpClient } from '@angular/common/http';

export abstract class BaseRestService {

  constructor(
    protected readonly http: HttpClient,
    protected readonly baseUrl: string,
  ) { }

  protected getUrl(route: string): string {
    return `${this.baseUrl}${route}`;
  }
}
