import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, map, catchError, of } from 'rxjs';
import { T_API } from '../config';
import { BaseRestService } from './base-rest.service';

@Injectable({
  providedIn: 'root'
})
export class HealthCheckService extends BaseRestService {

  constructor(
    http: HttpClient,
    @Inject(T_API) baseUrl: string,
  ) {
    super(http, baseUrl);
  }

  isBackendAlive(): Observable<boolean> {
    return this.http.get<any>(this.getUrl('/api/health/alive'))
      .pipe(
        map(() => true),
      );
  }
}
