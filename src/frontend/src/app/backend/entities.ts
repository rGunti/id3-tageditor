export interface PathResponse {
  path: string;
}

export interface File {
  name: string;
  size: number;
}
export interface FileListResponse extends PathResponse {
  files: File[];
  subDirectories: string[];
}

export interface Id3TagData {
  tagVersion: string,
  artists: string[],
  album: string | null,
  band: string | null,
  bpm: number | null,
  composers: string[],
  conductor: string | null,
  copyright: string | null,
  copyrightUrl: string | null,
  encoder: string | null,
  fileOwner: string | null,
  fileType: string | null,
  genre: string | null,
  lyricists: string[],
  publisher: string | null,
  title: string | null,
  track: number | null,
  trackCount: number | null,
  year: number | null,
  extendedMetaData?: { [key: string]: string | null },
}
export interface Id3TagResponse {
  path: string;
  data: Id3TagData | null;
}
