import { Id3TagData } from './entities';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FileListResponse, Id3TagResponse } from '.';
import { T_API } from '../config';
import { BaseRestService } from './base-rest.service';

@Injectable({
  providedIn: 'root'
})
export class FilesService extends BaseRestService {

  constructor(
    http: HttpClient,
    @Inject(T_API) baseUrl: string,
  ) {
    super(http, baseUrl);
  }

  private static sanitizePath(path?: string): string {
    if (path) {
      if (path.startsWith('/') || path.startsWith('\\')) {
        return path.substring(1);
      }
      return path;
    } else {
      return '';
    }
  }

  listFiles(path?: string): Observable<FileListResponse> {
    const params = new HttpParams()
      .set('path', FilesService.sanitizePath(path));
    return this.http.get<FileListResponse>(
      this.getUrl(`/api/files?${params.toString()}`));
  }

  getTagsForFile(path: string): Observable<Id3TagResponse> {
    const params = new HttpParams()
      .set('path', FilesService.sanitizePath(path));
    return this.http.get<Id3TagResponse>(
      this.getUrl(`/api/files/tag?${params.toString()}`));
  }

  updateTagsForFile(path: string, tags: Id3TagData): Observable<void> {
    const params = new HttpParams()
      .set('path', FilesService.sanitizePath(path));
    return this.http.post<void>(
      this.getUrl(`/api/files/tag?${params.toString()}`),
      tags);
  }
}
