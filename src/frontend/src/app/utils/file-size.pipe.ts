import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  private static unitLimits: [number, string, number][] = [
    [Math.pow(1024, 4), 'TB', 2],
    [Math.pow(1024, 3), 'GB', 2],
    [Math.pow(1024, 2), 'MB', 2],
    [Math.pow(1024, 1), 'kB', 2],
    [Math.pow(1024, 0), 'B', 0]
  ];

  transform(value: number | null | undefined, ...args: unknown[]): string {
    if (value === null || value === undefined) {
      return '-';
    }
    for (const mapping of FileSizePipe.unitLimits) {
      if (value >= mapping[0]) {
        return `${(value / mapping[0]).toLocaleString(undefined, {
          maximumFractionDigits: mapping[2]
        })} ${mapping[1]}`;
      }
    }
    return '-';
  }

}
