import { EditorEffects } from './editor';
import { FileListEffects } from './file-list/file-list.effects';
import { fileListReducer } from './file-list/file-list.reducer';
import { HealthCheckEffects } from './health-check/health-check.effects';
import { healthCheckReducer } from './health-check/health-check.reducer';

export * from './types';

export const reducers = {
  healthCheck: healthCheckReducer,
  fileList: fileListReducer
};
export const effects = [
  HealthCheckEffects,
  FileListEffects,
  EditorEffects,
];
