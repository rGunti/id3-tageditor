import { BaseState } from "../base";

export interface HealthCheckState extends BaseState {
  isOnline: boolean;
}
