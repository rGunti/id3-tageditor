import { Action, createReducer, on } from '@ngrx/store';
import { healthCheckActions } from '.';
import { HealthCheckState } from './entities';

export const initialHealthCheckState: HealthCheckState = {
  isOnline: false,
  isLoading: false,
  failureMessage: null,
  lastUpdated: null,
};

const _healthCheckReducer = createReducer(
  initialHealthCheckState,
  on(healthCheckActions.healthCheck, (state: HealthCheckState, _: any) => ({
    ...state,
    isLoading: true,
    failureMessage: null
  })),
  on(healthCheckActions.healthCheckSuccess, (state: HealthCheckState, response: { connected: boolean }) => ({
    ...state,
    isLoading: false,
    isOnline: response.connected,
    lastUpdated: new Date(),
  })),
  on(healthCheckActions.healthCheckFailure, (state: HealthCheckState, { message }) => ({
    ...state,
    isLoading: false,
    isOnline: false,
    failureMessage: message,
    lastUpdated: new Date(),
  }))
);

export function healthCheckReducer(state: HealthCheckState, action: Action): any {
  return _healthCheckReducer(state, action);
}
