import { createAction, props } from "@ngrx/store";

export const HEALTH_CHECK_UPDATE = '[Health Check] Update';
export const HEALTH_CHECK_SUCCESS = '[Health Check] Success';
export const HEALTH_CHECK_FAILURE = '[Health Check] Failure';

export const healthCheck = createAction(HEALTH_CHECK_UPDATE, props<any>());
export const healthCheckSuccess = createAction(HEALTH_CHECK_SUCCESS, props<{connected: boolean}>());
export const healthCheckFailure = createAction(HEALTH_CHECK_FAILURE, props<{message: string}>())
