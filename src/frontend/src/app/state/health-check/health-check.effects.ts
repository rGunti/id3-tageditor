import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, exhaustMap, map, of } from "rxjs";
import { HealthCheckService } from "src/app/backend";
import * as healthCheckActions from "./health-check.actions";

@Injectable()
export class HealthCheckEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly backend: HealthCheckService
  ) { }

  healthCheck$ = createEffect(() =>
  this.actions$.pipe(
    ofType(healthCheckActions.healthCheck),
    exhaustMap(_ =>
      this.backend.isBackendAlive().pipe(
        map(r => healthCheckActions.healthCheckSuccess({ connected: true })),
        catchError((e: Error) => of(healthCheckActions.healthCheckFailure({ message: e.message })))
      ))
  ));
}
