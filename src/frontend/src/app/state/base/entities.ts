export interface BaseState {
  isLoading: boolean;
  failureMessage: string | null;
  lastUpdated: Date | null;
}
