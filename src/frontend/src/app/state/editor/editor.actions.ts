import { Id3TagRequest } from './entities';
import { createAction, props } from '@ngrx/store';
import { Id3TagResponse } from 'src/app/backend';

export const editorUpdate = createAction('[Editor] Update', props<Id3TagRequest>());
export const editorUpdateSuccess = createAction('[Editor] Update Success', props<Id3TagResponse>());
export const editorUpdateFailure = createAction('[Editor] Update Failure', props<Error>());

export const updateId3Tag = createAction('[Editor] Update ID3 Tag', props<Id3TagResponse>());
export const updateId3TagSuccess = createAction('[Editor] Update ID3 Tag Success', props<any>());
export const updateId3TagFailure = createAction('[Editor] Update ID3 Tag Failure', props<Error>());

export const none = createAction('[Editor] None');
