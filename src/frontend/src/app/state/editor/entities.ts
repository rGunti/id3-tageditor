import { Id3TagData } from 'src/app/backend';
import { BaseState } from './../base/entities';

export interface EditorState extends BaseState {
  isWriting: boolean;
  tagData: Id3TagData | null;
}

export interface Id3TagRequest {
  path: string;
}
