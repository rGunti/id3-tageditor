import { createReducer, Action, on } from '@ngrx/store';
import { editorActions, EditorState, Id3TagRequest } from ".";
import { Id3TagResponse } from 'src/app/backend';

export const initialEditorState: EditorState = {
  isLoading: false,
  isWriting: false,
  failureMessage: null,
  lastUpdated: null,
  tagData: null,
};

const _reducer = createReducer(
  initialEditorState,
  on(editorActions.editorUpdate, (state: EditorState, p: Id3TagRequest) => ({
    ...state,
    isLoading: true,
    failureMessage: null,
  })),
  on(editorActions.editorUpdateSuccess, (state: EditorState, p: Id3TagResponse) => ({
    ...state,
    isLoading: false,
    lastUpdated: new Date(),
    tagData: p.data,
  })),
  on(editorActions.editorUpdateFailure, (state: EditorState, { message }) => ({
    ...state,
    isLoading: false,
    lastUpdated: new Date(),
    failureMessage: message,
  })),
  on(editorActions.updateId3Tag, (state: EditorState, p: Id3TagRequest) => ({
    ...state,
    isWriting: true,
  })),
  on(editorActions.updateId3TagSuccess, (state: EditorState, _: any) => ({
    ...state,
    isWriting: false,
  })),
  on(editorActions.updateId3TagFailure, (state: EditorState, { message }) => ({
    ...state,
    isWriting: false,
    failureMessage: message
  })));

export function editorReducer(state: EditorState, action: Action): any {
  return _reducer(state, action);
}
