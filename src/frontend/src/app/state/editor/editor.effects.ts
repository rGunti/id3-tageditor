import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { exhaustMap, map, catchError, of, filter, tap } from 'rxjs';
import { ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { createEffect } from '@ngrx/effects';
import { FilesService } from './../../backend/files.service';
import { editorActions } from '.';
import { GlobalState } from '..';
import { TagEditorDialogComponent } from 'src/app/dialogs/tag-editor-dialog/tag-editor-dialog.component';
import { fileListActions } from '../file-list';
import { Store } from '@ngrx/store';

@Injectable()
export class EditorEffects {
  private currentFileListPath: string = '';

  constructor(
    private readonly actions$: Actions,
    private readonly backend: FilesService,
    private readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar,
    private readonly store: Store<GlobalState>,
  ) {
    store.select('fileList').pipe(
      map(r => r.currentPath)
    ).subscribe(p => this.currentFileListPath = p);
  }

  updateEditor$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editorActions.editorUpdate),
      exhaustMap(p =>
        this.backend.getTagsForFile(p.path).pipe(
          map(r => editorActions.editorUpdateSuccess(r)),
          catchError((e: Error) => of(editorActions.editorUpdateFailure(e)))
        ))
    ));

  updateEditorSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editorActions.editorUpdateSuccess),
      exhaustMap(response =>
        TagEditorDialogComponent.open(this.dialog, response).pipe(
          filter(r => r !== null),
          map(r => editorActions.updateId3Tag(r))
        ))
    ));

  updateEditorFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editorActions.editorUpdateFailure),
      tap(error => this.snackBar.open(`Failed to open file: ${error.message}`, 'OK', { duration: 2500 }))
    ),
    { dispatch: false });

  updateId3Tag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editorActions.updateId3Tag),
      filter(p => !!p.data),
      exhaustMap(p =>
        this.backend.updateTagsForFile(p.path, p.data!).pipe(
          map(path => fileListActions.fileListUpdate({ path: this.currentFileListPath })),
          catchError((e: Error) => of(editorActions.updateId3TagFailure(e)))
        ))
    ));
}
