import { File } from "src/app/backend";
import { BaseState } from "../base";

export interface UpdateFileListParams {
  path?: string;
}

export interface FileListState extends BaseState {
  currentPath: string;
  subDirectories: string[];
  files: File[];
}
