import { fileListActions } from '.';
import { FilesService } from './../../backend/files.service';
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Injectable } from '@angular/core';
import { catchError, exhaustMap, map, of } from 'rxjs';

@Injectable()
export class FileListEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly backend: FilesService
  ) {
  }

  updateFileList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fileListActions.fileListUpdate),
      exhaustMap(p =>
        this.backend.listFiles(p.path).pipe(
          map(r => fileListActions.fileListUpdateSuccess(r)),
          catchError((e: Error) => of(fileListActions.fileListUpdateFailure(e)))
        ))
    ));
}
