import { Action, createReducer, on } from '@ngrx/store';
import { FileListResponse } from 'src/app/backend';
import { fileListActions, FileListState } from ".";

export const initialFileListState: FileListState = {
  isLoading: false,
  failureMessage: null,
  lastUpdated: null,
  currentPath: '',
  files: [],
  subDirectories: [],
};

const _reducer = createReducer(
  initialFileListState,
  on(fileListActions.fileListUpdate, (state: FileListState, _) => ({
    ...state,
    isLoading: true,
    failureMessage: null,
  })),
  on(fileListActions.fileListUpdateSuccess, (state: FileListState, response: FileListResponse) => ({
    ...state,
    isLoading: false,
    lastUpdated: new Date(),
    currentPath: response.path,
    files: response.files,
    subDirectories: response.subDirectories,
  })),
  on(fileListActions.fileListUpdateFailure, (state: FileListState, { message }) => ({
    ...state,
    isLoading: false,
    lastUpdated: new Date(),
    failureMessage: message,
  }))
);

export function fileListReducer(state: FileListState, action: Action): any {
  return _reducer(state, action);
}
