import { props } from '@ngrx/store';
import { createAction } from '@ngrx/store';
import { FileListResponse } from 'src/app/backend';
import { UpdateFileListParams } from '.';

export const fileListUpdate = createAction('[File List] Update', props<UpdateFileListParams>());
export const fileListUpdateSuccess = createAction('[File List] Update Success', props<FileListResponse>());
export const fileListUpdateFailure = createAction('[File List] Update Failure', props<Error>());
