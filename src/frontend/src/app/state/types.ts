import { FileListState } from "./file-list";
import { HealthCheckState } from "./health-check";

export interface GlobalState {
  healthCheck: HealthCheckState;
  fileList: FileListState;
}
