# Introduction

ID3 Tag Editor (_ID3TE_) allows you to edit metadata of music files in your web browser.

![ID3 Tag Editor](./id3te-editor.png)
![Editor Dialog](./id3te-editor-edit.png)

## Intended Use Case
The intended use case for ID3TE is to tag downloaded content from platforms like YouTube using tools like `yt-dlp` using only web technologies so this process can be performed from a phone or a computer.

_If you are interested in my personal setup, check out the [Example project][example-project] here._

## Installation
To run ID3TE, it is recommended that you use the provided [`docker-compose.yaml` file][docker-compose].

```yaml
version: '3'
services:
  gateway:
    image: registry.gitlab.com/rgunti/id3-tageditor/gateway:latest
    container_name: id3te-gateway
    ports:
      - 8080:80  # <-- Set your own port here
    depends_on:
      - ui
      - backend
  ui:
    container_name: id3te-ui
    image: registry.gitlab.com/rgunti/id3-tageditor/ui:latest
    depends_on:
      - backend
  backend:
    container_name: id3te-backend
    image: registry.gitlab.com/rgunti/id3-tageditor/server:latest
    volumes:
      - /path/to/your/data:/data:rw  # <-- Set your own path here
```

Place this configuration file in a folder on your server and then run `docker-compose up -d` to pull and start.

::: warning
Do not edit the `container_name` properties. These are hardwired to be used in this setup.
If you want to use custom `container_name`s, please replace the gateway container with your own reverse proxy (like `nginx`) and apply the needed configuration. You may find the used configuration file in the projects source code ([see below](#gateway)).
:::

### Configuration
You can configure the backend using environment variables in the `docker-compose.yaml` file. The environment variables start with `ID3TE__`. See the [configuration file reference][config-ref] for details.

### Update
To update your installation, run `docker-compose pull`. Then run `docker-compose up -d` to restart all updated containers.

### Run outside Docker
The officially supported way to run ID3TE is to run Docker containers. However if you want to run ID3TE on "bare metal", you may choose to download the build artifacts from the [pipelines page on GitLab][gitlab-pipelines] on the `master` branch.
You will need to install `dotnet` and a web server with reverse proxy support (nginx or Apache httpd). You can find a configuration file for reference in the source code ([see below](#gateway)).

## Architecture
ID3TE comes in three parts, each serving a separate function:

|          | Container Name                                     |
|:---------|:---------------------------------------------------|
| Backend  | `registry.gitlab.com/rgunti/id3-tageditor/server`  |
| Frontend | `registry.gitlab.com/rgunti/id3-tageditor/ui`      |
| Gateway  | `registry.gitlab.com/rgunti/id3-tageditor/gateway` |

The recommended way to operate ID3TE is through Docker and `docker-compose`.

::: tip On Containers and Docker Hub
ID3TE is still in early development phase. As such, any `master` build is published on the GitLab Container Registry as a `latest` build. Once more stable releases can be provided, they will be published on Docker Hub.
:::

### Backend ("Server")
The backend lists files and performs edits on them. As such, it needs access to a dedicated folder on your filesystem where these files are stored.

_The backend is built using **.NET 6**._

### Frontend ("UI")
The frontend is a web application being served by an _nginx_ web server and provides the user interface. It connects to the backend via its RESTful API.

_The frontend is built using **Angular**._

### Gateway
The gateway container is an optional, but recommended, part of ID3TE which can be used in `docker-compose` to string together the backend and frontend.
Again, this is an `nginx` web server acting as a reverse proxy. It comes preconfigured for the above mentioned use case. The configuration file being applied here is available in the projects source code under `src/gateway/`.

[config-ref]: /id3-tageditor/config
[docker-compose]: https://gitlab.com/rGunti/id3-tageditor/-/blob/master/docker-compose.yml
[example-project]: https://domain.invalid
[gitlab-pipelines]: https://gitlab.com/rGunti/id3-tageditor/-/pipelines?scope=branches&page=1
