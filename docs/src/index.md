---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: A web-based ID3 tag editor for the homelab
actions:
- text: Quick Start →
  link: /guide/
  type: primary
- text: Config Reference
  link: /config/
  type: secondary
features:
- title: Homelab friendly
  details: >-
    ID3 Tag Editor is made with the homelab in mind. It's plug and play with sane defaults and gives plenty of options to integrate in your lab.
- title: Container-native
  details: >-
    Coming with Docker images and a docker-compose.yml by default, all you need to do is put it on your computer or server and run it. And if you don't like it, it's gone without leaving nasty traces behind.
- title: Stupid Simple
  details: >-
    Easy to use, easy to master!
    Yeah, that's it!
footer: Made with ❤️ in 🇩🇪 by a 🇨🇭 dude and licensed under MIT
---
