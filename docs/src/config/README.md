---
sidebar: auto
---

# Backend Configuration
The backend can be configured via environment variables prefixed by `ID3TE__`.

## Directory

`ID3TE__Directory`

- Type: `string`
- Default: `/data`

Configures the directory where media files are stored.

## FilterForExtensions

`ID3TE__FilterForExtensions`

- Type: `string[]`
- Default: _see below_

A list of extensions to filter for. For details about configuring arrays, please refer to the official [dotnet documentation][dotnet-config].

The factory default is defined as follows:
```
ID3TE__FilterForExtensions__0=.mp3
ID3TE__FilterForExtensions__1=.flac
```

[dotnet-config]: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-6.0#environment-variables
