const { description } = require('../../package')

module.exports = {
  base: '/id3-tageditor/',
  title: 'ID3 Tag Editor',
  description: description,

  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  theme: '@vuepress/theme-default',
  themeConfig: {
    repo: 'https://gitlab.com/rGunti/id3-tageditor/',
    docsDir: 'docs',
    editLink: false,
    editLinkText: 'Edit this page',
    navbar: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Config',
        link: '/config/'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '/guide/README.md',
          ]
        }
      ],
      '/config/': [
        {
          title: 'Config References',
          collapsable: true,
        }
      ]
    }
  },

  plugins: [
    [
      '@vuepress/plugin-search',
      {
        locales: {
          '/': {
            placeholder: 'Search'
          }
        }
      }
    ]
  ]
}
