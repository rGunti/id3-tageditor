# 🎵 ID3 Tag Editor 🎵
ID3 Tag Editor is a web-based ID3 tag editor mainly designed for homelab use.

> ⚠️ **IMPORTANT**: This project currently does not feature any form of authentication.
> It is therefore recommended to deploy this app only in a trusted environment.
> Authentication will (maybe) come in later but it sits lower on the priority list
> as this project is purely intended for homelab use and should not be deployed
> online.

## ❓ Intended Use Case
When downloading music files from the internet, sometimes these files do not contain
good enough (or any) tagging data which makes sorting them really difficult. And editing
this metadata requires seperate software for each platform.

ID3 Tag Editor aims to solve this using a web-based solution, so it can be deployed in a
homelab and used from any device on the network. It is best used with other webservices,
like [MeTube](https://github.com/alexta69/metube) (a web-frontend for
[`yt-dlp`](https://github.com/yt-dlp/yt-dlp) / [`youtube-dl`](https://github.com/ytdl-org/youtube-dl)).

## 🧩 Features
- List MP3 files in directory (and subdirectories)
- Display of ID3 tags embedded in MP3 files
- Modification of ID3 tags in MP3 files

## 🖥️ Technology Stack
- Backend: dotnet v6
- Frontend: Angular v13
- Container: Docker

## ✅ TODOs
TODOs have been moved to [GitLab Issues][gitlab-issues].

## 📜 License
This project is licensed under the MIT License.
You can find more information in the [LICENSE](LICENSE) file.

[gitlab-issues]: https://gitlab.com/rGunti/id3-tageditor/-/issues
